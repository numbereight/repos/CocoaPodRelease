Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.ios.deployment_target = '12.0'

  spec.name = 'NumberEightCompiled'
  spec.module_name = 'NumberEightCompiled'
  spec.version = '3.12.1'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.ai/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = {
    :http => "https://storage.googleapis.com/numbereight-release-sdk/ios/nesdk/3.12.1/NumberEightCompiled.xcframework.zip",
  }
  spec.vendored_frameworks = 'NumberEightCompiled.xcframework'
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.swift_versions = [ '5.0' ]
end
