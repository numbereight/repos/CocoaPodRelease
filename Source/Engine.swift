//
//  Engine.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension Engine {
    static func parameterListToParameters(params: [Parameters]) -> Parameters? {
        guard !params.isEmpty else { return nil }

        return params.reduce(Parameters(filter: "")) { (result, next) -> Parameters in
            return result.and(next)
        }
    }

    // swiftlint:disable cyclomatic_complexity
    static func topicFromSensorItem(type: NEXSensorItem.Type) -> String? {
        switch type {
        case is NEXTime.Type:
            return kNETopicTime
        case is NEXPlace.Type:
            return kNETopicPlace
        case is NEXWeather.Type:
            return kNETopicWeather
        case is NEXActivity.Type:
            return kNETopicActivity
        case is NEXLocation.Type:
            return kNETopicLocation
        case is NEXMovement.Type:
            return kNETopicDeviceMovement
        case is NEXProximity.Type:
            return kNETopicProximity
        case is NEXReachability.Type:
            return kNETopicReachability
        case is NEXSituation.Type:
            return kNETopicSituation
        case is NEXLockStatus.Type:
            return kNETopicLockStatus
        case is NEXAmbientLight.Type:
            return kNETopicAmbientLight
        case is NEXGyroscopeData.Type:
            return kNETopicGyroscope
        case is NEXIndoorOutdoor.Type:
            return kNETopicIndoorOutdoor
        case is NEXDevicePosition.Type:
            return kNETopicDevicePosition
        case is NEXActivity.Type:
            return kNETopicActivity
        case is NEXAmbientPressure.Type:
            return kNETopicAmbientPressure
        case is NEXLocationCluster.Type:
            return kNETopicLocationCluster
        case is NEXMagneticVariance.Type:
            return kNETopicMagneticVariance
        case is NEXMagnetometerData.Type:
            return kNETopicMagnetometer
        case is NEXScreenBrightness.Type:
            return kNETopicScreenBrightness
        case is NEXAccelerometerData.Type:
            return kNETopicAccelerometer
        case is NEXLocationClusterID.Type:
            return kNETopicLocationClusterID
        default:
            return nil
        }
    }
    // swiftlint:enable cyclomatic_complexity

    @discardableResult
    public func subscribe<T: NEXSensorItem>(to: String? = nil, parameters: Parameters? = nil,
                                            callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) -> Subscription? {
        guard let topic = to ?? Engine.topicFromSensorItem(type: T.self) else {
            NELog.msg(Engine.LOG_TAG, warning: "Cannot determine topic for type (\(T.self.description())). Unable to subscribe.")
            return nil
        }

        guard let handler = GlimpseHandler<T>(block: callback) as? GlimpseHandler<NEXSensorItem> else {
            NELog.msg(Engine.LOG_TAG, warning: "Cannot cast callback to GlimpseHandler<T>. Unable to subscribe to topic: \(topic)")
            return nil
        }
        return self.__subscribe(toTopic: topic, parameters: parameters, handler: handler)
    }

    @discardableResult
    public func subscribe<T: NEXSensorItem>(to: String? = nil, parameters: [Parameters],
                                            callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) -> Subscription? {
        return self.subscribe(to: to,
                              parameters: Engine.parameterListToParameters(params: parameters),
                              callback: callback)
    }

    public func request<T: NEXSensorItem>(forTopic: String? = nil, parameters: Parameters? = nil,
                                          callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) {
        guard let topic = forTopic ?? Engine.topicFromSensorItem(type: T.self) else {
            NELog.msg(Engine.LOG_TAG, warning: "Cannot determine topic for type (\(T.self.description())). Unable to perform request.")
            return
        }

        guard let handler = GlimpseHandler<T>(block: callback) as? GlimpseHandler<NEXSensorItem> else {
            NELog.msg(Engine.LOG_TAG, warning: "Cannot cast callback to GlimpseHandler<T>. Unable to perform request.")
            return
        }

        self.__request(forTopic: topic, parameters: parameters, handler: handler)
    }

    public func request<T: NEXSensorItem>(forTopic: String? = nil, parameters: [Parameters],
                                          callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) {
        self.request(forTopic: forTopic,
                     parameters: Engine.parameterListToParameters(params: parameters),
                     callback: callback)
    }
}
