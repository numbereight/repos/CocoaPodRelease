//
//  Parameters.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-29.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension Parameters {
    public static func | (lhs: Parameters, rhs: Parameters) -> Parameters {
        return lhs.and(rhs)
    }
}
