//
//  NumberEight.h
//  NumberEight
//
//  Created by Oliver Kocsis on 30/09/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for NumberEight.
FOUNDATION_EXPORT double NumberEightVersionNumber;

//! Project version string for NumberEight.
FOUNDATION_EXPORT const unsigned char NumberEightVersionString[];

#import <NumberEightCompiled/NumberEightCompiled.h>
