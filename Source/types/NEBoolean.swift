//
//  NEBoolean.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-05-11.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NEBoolean: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.value = try decCont.decode(Bool.self, forKey: .value)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.value, forKey: .value)
    }

    enum CodingKeys: String, CodingKey {
        case value
    }
}

#if compiler(>=6.0)
extension NEBoolean: @retroactive Equatable {
    public static func == (inLhs: NEBoolean, inRhs: NEBoolean) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEBoolean_isEqual(&lhs, &rhs)
    }
}
#else
extension NEBoolean: Equatable {
    public static func == (inLhs: NEBoolean, inRhs: NEBoolean) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEBoolean_isEqual(&lhs, &rhs)
    }
}
#endif
