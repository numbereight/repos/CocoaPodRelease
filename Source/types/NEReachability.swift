//
//  NEReachability.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEReachabilityCellState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityCellState in
            return NEReachability_cellStateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromCellState(self))
    }
}
#else
extension NEReachabilityCellState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityCellState in
            return NEReachability_cellStateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromCellState(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEReachabilityCellDataState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityCellDataState in
            return NEReachability_cellDataStateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromCellDataState(self))
    }
}
#else
extension NEReachabilityCellDataState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityCellDataState in
            return NEReachability_cellDataStateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromCellDataState(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEReachabilityWifiState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityWifiState in
            return NEReachability_wifiStateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromWifiState(self))
    }
}
#else
extension NEReachabilityWifiState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityWifiState in
            return NEReachability_wifiStateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromWifiState(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEReachabilityFlag: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityFlags in
            return NEReachability_flagsFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromFlags(self))
    }
}
#else
extension NEReachabilityFlag: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEReachabilityFlags in
            return NEReachability_flagsFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEReachability_reprFromFlags(self))
    }
}
#endif

extension NEReachability: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.cellState = NEReachabilityCellState(
            try decCont.decode(String.self, forKey: .cellState)) ?? .unknown
        self.cellDataState = NEReachabilityCellDataState(
            try decCont.decode(String.self, forKey: .cellDataState)) ?? .unknown
        self.wifiState = NEReachabilityWifiState(
            try decCont.decode(String.self, forKey: .wifiState)) ?? .unknown
        self.flags = NEReachabilityFlag(
            try decCont.decode(String.self, forKey: .flags)) ?? NEReachabilityFlag()
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.cellState.description, forKey: .cellState)
        try encCont.encode(self.cellDataState.description, forKey: .cellDataState)
        try encCont.encode(self.wifiState.description, forKey: .wifiState)
        try encCont.encode(self.flags.description, forKey: .flags)
    }

    enum CodingKeys: String, CodingKey {
        case cellState
        case cellDataState
        case wifiState
        case flags
    }
}

#if compiler(>=6.0)
extension NEReachability: @retroactive Equatable {
    public static func == (inLhs: NEReachability, inRhs: NEReachability) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEReachability_isEqual(&lhs, &rhs)
    }
}
#else
extension NEReachability: Equatable {
    public static func == (inLhs: NEReachability, inRhs: NEReachability) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEReachability_isEqual(&lhs, &rhs)
    }
}
#endif
