//
//  Signal.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NESignal: Codable {

    var baseStationIDStr: String {
        get {
            var signal = self
            return withUnsafePointer(to: &signal.baseStationID) {
                let capacity = MemoryLayout.size(ofValue: self.baseStationID)
                return $0.withMemoryRebound(to: UInt8.self, capacity: capacity) { (charPtr: UnsafePointer<UInt8>) -> String in
                    return String(cString: charPtr)
                }
            }
        }
        set {
            withUnsafeMutablePointer(to: &self) { (selfPtr: UnsafeMutablePointer<NESignal>) -> Void in
                let strLen = newValue.utf8.count
                newValue.withCString { (cstrPtr: UnsafePointer<Int8>) -> Void in
                    NESignal_setBaseStationID(selfPtr, cstrPtr, strLen)
                }
            }
        }
    }

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.baseStationIDStr = try decCont.decode(String.self, forKey: .baseStationID)
        self.strength = try decCont.decode(Int32.self, forKey: .strength)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.baseStationIDStr, forKey: .baseStationID)
        try encCont.encode(self.strength, forKey: .strength)
    }

    enum CodingKeys: String, CodingKey {
        case baseStationID
        case strength
    }
}

#if compiler(>=6.0)
extension NESignal: @retroactive Equatable {
    public static func == (inLhs: NESignal, inRhs: NESignal) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NESignal_isEqual(&lhs, &rhs)
    }
}
#else
extension NESignal: Equatable {
    public static func == (inLhs: NESignal, inRhs: NESignal) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NESignal_isEqual(&lhs, &rhs)
    }
}
#endif
