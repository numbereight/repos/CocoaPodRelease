//
//  NEWeather.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEWeatherTemperature: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEWeatherTemperature in
            return NEWeather_temperatureFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEWeather_reprFromTemperature(self))
    }
}
#else
extension NEWeatherTemperature: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEWeatherTemperature in
            return NEWeather_temperatureFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEWeather_reprFromTemperature(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEWeatherConditions: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEWeatherConditions in
            return NEWeather_conditionsFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEWeather_reprFromConditions(self))
    }
}
#else
extension NEWeatherConditions: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEWeatherConditions in
            return NEWeather_conditionsFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEWeather_reprFromConditions(self))
    }
}
#endif

extension NEWeather: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.temperature = NEWeatherTemperature(
            try decCont.decode(String.self, forKey: .temperature)) ?? .unknown
        self.conditions = NEWeatherConditions(
            try decCont.decode(String.self, forKey: .conditions)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(temperature.description, forKey: .temperature)
        try encCont.encode(conditions.description, forKey: .conditions)
    }

    enum CodingKeys: String, CodingKey {
        case temperature
        case conditions
    }
}

#if compiler(>=6.0)
extension NEWeather: @retroactive Equatable {
    public static func == (inLhs: NEWeather, inRhs: NEWeather) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEWeather_isEqual(&lhs, &rhs)
    }
}
#else
extension NEWeather: Equatable {
    public static func == (inLhs: NEWeather, inRhs: NEWeather) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEWeather_isEqual(&lhs, &rhs)
    }
}
#endif
